import { fizzBuzz } from "../src/fizzBuzz.js"

describe(
    "Je teste la méthode Fizzbuzz", () => {

        it("je teste en entrée si c'est bien un entier", () => {

        //arrange
        const integer:number = 3; 

        //act
        const toto = fizzBuzz(integer);
        
        //assert
        expect(typeof toto).toBe("string");
    })
    it("Je teste que l'entrée est un multiple de 3 en sortie cela affiche 'fizz'", () => {
        //arrange
       const integer:number = 3;

       //act
       const toto = fizzBuzz(integer);
       //assert
       expect(toto).toBe("fizz");
    }
    )
    it("Je teste que l'entrée est un multiple de 5 en sortie cela affiche buzz", () => {
        //arrange
       const integer:number = 5;

       //act
       const toto = fizzBuzz(integer);
       //assert
       expect(toto).toBe("buzz");
    }
    )
    it("Je teste que l'entrée est un multiple de 3 et de 5 en sortie cela affiche fizzbuzz", () => {
        //arrange
       const integer:number = 15;

       //act
       const toto = fizzBuzz(integer);
       //assert
       expect(toto).toBe("fizzbuzz");
    }
    )
    })